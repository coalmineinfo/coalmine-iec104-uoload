package com.coalmine.iec104.config.constant;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;

/**
 * @Author mkq
 * @Date 2023-10-30 11:32
 * @Description
 */
@Configuration
@ConfigurationProperties(prefix = "iec104")
@Data
public class IecConfig {

    private String url;

    private Integer port;

    private Integer commonAddress;

    private Integer uploadRate;

    private Integer serverPort;

    private List<IecClientInfo> collectClient;

}
