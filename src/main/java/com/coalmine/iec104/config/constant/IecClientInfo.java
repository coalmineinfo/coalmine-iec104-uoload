package com.coalmine.iec104.config.constant;

import lombok.Data;

import java.util.Objects;

/**
 * @Author mkq
 * @Date 2023-11-06 9:38
 * @Description
 */
@Data
public class IecClientInfo {

    private String tag;

    private String ipAddress;

    private Integer port;

    private Integer commonAddress;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IecClientInfo that = (IecClientInfo) o;
        return Objects.equals(tag, that.tag) && Objects.equals(ipAddress, that.ipAddress) && Objects.equals(port, that.port) && Objects.equals(commonAddress, that.commonAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tag, ipAddress, port, commonAddress);
    }
}
