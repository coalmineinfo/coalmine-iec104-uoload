package com.coalmine.iec104.modules.controller;

import com.coalmine.iec104.modules.domain.R;
import com.coalmine.iec104.modules.service.ReportDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * 导入模板数据Controller
 *
 * @author wxy
 * @date 2023-10-30
 */
@RestController
@RequestMapping("/reportData")
@Api(tags = "导入模板数据")
public class ReportDataController {

    @Autowired
    private ReportDataService reportDataService;

    /**
     * 导入文档数据
     */
    @ApiOperation("导入文档数据")
    @PostMapping("/importReportData")
    public R<?> importReportData(MultipartFile file) throws Exception {
        return R.ok(reportDataService.importReportData(file));
    }

    /**
     * 获取文档数据
     */
    @ApiOperation("获取文档数据")
    @PostMapping("/getReportData")
    public R<?> getReportData(){
        return R.ok(reportDataService.getReportData());
    }

    /**
     * 从根目录获取文件
     */
    @ApiOperation("从根目录获取文件")
    @PostMapping("/getUrlReportData")
    public R<?> getUrlReportData() throws Exception {
        return R.ok(reportDataService.getUrlReportData());
    }

}
