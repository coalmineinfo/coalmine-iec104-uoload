package com.coalmine.iec104.modules.handle;

import com.coalmine.iec104.modules.delayed.entity.ItemDelayed;
import com.coalmine.iec104.modules.delayed.service.IDelayItem;
import com.coalmine.iec104.modules.domain.UploadMsgInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.concurrent.DelayQueue;

/**
 * @Author mkq
 * @Date 2023-10-30 14:29
 * @Description
 */
@Slf4j
@Lazy(false)
@Component
public class DelayMsgImpl implements IDelayItem<UploadMsgInfo> {

    private final static DelayQueue<ItemDelayed<UploadMsgInfo>> ASDU_QUEUE = new DelayQueue<>();


    @Override
    public boolean addToDelayQueue(ItemDelayed<UploadMsgInfo> itemDelayed) {
        return ASDU_QUEUE.add(itemDelayed);
    }

    @Override
    public boolean addToDelayQueue(UploadMsgInfo data) {
        ItemDelayed<UploadMsgInfo> delayedItem = new ItemDelayed<>(System.currentTimeMillis(), 2);
        delayedItem.setData(data);
        return ASDU_QUEUE.add(delayedItem);
    }

    @Override
    public void removeDelayQueue(UploadMsgInfo data) {

    }


    public DelayQueue<ItemDelayed<UploadMsgInfo>> getAsduQueue() {
        return ASDU_QUEUE;
    }

}
