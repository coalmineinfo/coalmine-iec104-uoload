package com.coalmine.iec104.modules.service;

import com.coalmine.iec104.modules.domain.ReportDate;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 模板数据Service
 *
 * @author wxy
 * @date 2023-10-30
 */
public interface ReportDataService {

    List<ReportDate> importReportData(MultipartFile file) throws Exception;

    List<ReportDate> getReportData();

    List<ReportDate> getUrlReportData() throws Exception;
}
