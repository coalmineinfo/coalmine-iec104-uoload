package com.coalmine.iec104.modules.service.impl;

import com.coalmine.iec104.config.constant.Constants;
import com.coalmine.iec104.modules.domain.ReportDate;
import com.coalmine.iec104.modules.service.ReportDataService;
import com.coalmine.iec104.modules.util.ExcelUtil;
import com.coalmine.iec104.modules.util.RedisCache;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

/**
 * 模板数据Service实现类
 *
 * @author wxy
 * @date 2023-10-30
 */
@Service
public class ReportDataServiceImpl implements ReportDataService {

    @Resource
    private RedisCache redisCache;

    @Value("${upload.url}")
    private String path;

    @Override
    public List<ReportDate> importReportData(MultipartFile file) throws Exception {
        ArrayList<ReportDate> reportDates = new ArrayList<>();
        ExcelUtil<ReportDate> util = new ExcelUtil<>(ReportDate.class);
        List<ReportDate> reportDate1 = util.importExcel("遥测", file.getInputStream());
        if (CollectionUtils.isNotEmpty(reportDate1)) {
            reportDate1.forEach(u -> u.setType("1"));
            reportDates.addAll(reportDate1);
        }
        List<ReportDate> reportDate2 = util.importExcel("遥信", file.getInputStream());
        if (CollectionUtils.isNotEmpty(reportDate2)) {
            reportDate2.forEach(u -> u.setType("2"));
            reportDates.addAll(reportDate2);
        }
        List<ReportDate> reportDate3 = util.importExcel("电度量", file.getInputStream());
        if (CollectionUtils.isNotEmpty(reportDate3)) {
            reportDate3.forEach(u -> u.setType("3"));
            reportDates.addAll(reportDate3);
        }
        redisCache.deleteObject(Constants.REPORT_DATA_KEY);
        redisCache.setCacheList(Constants.REPORT_DATA_KEY, reportDates);
        return reportDates;
    }

    private List<ReportDate> handleData(List<ReportDate> reportDates, InputStream inputStream) throws Exception {
        ExcelUtil<ReportDate> util = new ExcelUtil<>(ReportDate.class);
        List<String> sheets = Arrays.asList("遥测", "遥信", "电度量");
        HashMap<String, List<ReportDate>> map = util.importExcelOwn(sheets, inputStream);
        map.forEach((k, v) -> {
            if (StringUtils.equals("遥测", k)) {
                v.forEach(u -> u.setType("1"));
                reportDates.addAll(v);
            } else if (StringUtils.equals("遥信", k)) {
                v.forEach(u -> u.setType("2"));
                reportDates.addAll(v);
            } else if (StringUtils.equals("电度量", k)) {
                v.forEach(u -> u.setType("3"));
                reportDates.addAll(v);
            }
        });
        redisCache.deleteObject(Constants.REPORT_DATA_KEY);
        redisCache.setCacheList(Constants.REPORT_DATA_KEY, reportDates);
        return reportDates;
    }

    @Override
    public List<ReportDate> getReportData() {
        return redisCache.getCacheList(Constants.REPORT_DATA_KEY);
    }

    @Override
    public List<ReportDate> getUrlReportData() throws Exception {
        ArrayList<ReportDate> reportDates = new ArrayList<>();
        FileInputStream file = new FileInputStream(path);
        return handleData(reportDates, file);
    }

}
