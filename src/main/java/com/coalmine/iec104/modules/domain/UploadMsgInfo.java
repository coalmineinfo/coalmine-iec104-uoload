package com.coalmine.iec104.modules.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.openmuc.j60870.ASdu;

/**
 * @Author mkq
 * @Date 2023-11-08 14:00
 * @Description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class UploadMsgInfo {

    private String msgType;

    private ASdu asdu;
}
