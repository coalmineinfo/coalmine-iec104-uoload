package com.coalmine.iec104.modules.domain;


import com.coalmine.iec104.annotation.Excel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 导入excel实体类
 *
 * @author wxy
 * @date 2023-10-30
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ReportDate implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;

    /**
     * 系统名称
     */
    @Excel(name = "系统名称")
    private String systemName;

    /**
     * 系统编码
     */
    @Excel(name = "系统编码")
    private String systemCode;

    /**
     * 点号
     */
    @Excel(name = "点号")
    private String pointCode;

    /**
     * 点位名称
     */
    @Excel(name = "名称")
    private String pointName;

    /**
     * 单位
     */
    @Excel(name = "单位")
    private String unit;

    /**
     * 系数
     */
    @Excel(name = "系数")
    private String coefficient;

    /**
     * 对应redis中的key
     */
    @Excel(name = "标识")
    private String pointKey;

    /**
     * 类型 1-遥测 2-遥信 3-点度量
     */
    private String type;
}
