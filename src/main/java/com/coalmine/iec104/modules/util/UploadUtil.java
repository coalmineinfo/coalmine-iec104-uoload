package com.coalmine.iec104.modules.util;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

/**
 * @Author mkq
 * @Date 2023-11-08 13:36
 * @Description
 */
public class UploadUtil {
    private static final RestTemplate restTemplate = SpringUtils.getBean(RestTemplate.class);


    public static void saveUploadData(String url, HashMap<String, Object> map) {
        if (StringUtils.isNotBlank(url)) {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            //用HttpEntity封装整个请求报文
            HttpEntity<HashMap<String, Object>> request = new HttpEntity<>(map, headers);
            JSONObject jsonObject = restTemplate.postForObject(url + "/upload/iec/info", request, JSONObject.class);
        }
    }


}
