package com.coalmine.iec104.modules.util;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * ID生成器工具类
 *
 */
public class IdUtils {
    /**
     * 获取随机UUID
     *
     * @return 随机UUID
     */
    public static String randomUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线
     *
     * @return 简化的UUID，去掉了横线
     */
    public static String simpleUUID() {
        return UUID.randomUUID().toString(true);
    }

    /**
     * 获取随机UUID，使用性能更好的ThreadLocalRandom生成UUID
     *
     * @return 随机UUID
     */
    public static String fastUUID() {
        return UUID.fastUUID().toString();
    }

    /**
     * 简化的UUID，去掉了横线，使用性能更好的ThreadLocalRandom生成UUID
     *
     * @return 简化的UUID，去掉了横线
     */
    public static String fastSimpleUUID() {
        return UUID.fastUUID().toString(true);
    }


    public static String getOddNumbers(String maxOddNumbers) {
        //定义变量(最新的单号)
        String newOddNumber = "";
        //获取当前日期并将其进行格式化
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyyMMdd");
        String formatDate = simpleDateFormat.format(new Date());
        //判断数据中的最大单号是否存在，是否包含当前日期
        if (maxOddNumbers != null && maxOddNumbers.contains(formatDate)) {
            //截取后四位数
            String endNum = maxOddNumbers.substring(8);
            //把截取的最后四位数解析为int
            int endIntNum = Integer.parseInt(endNum);
            //在将其加1(自增1)
            int newEndIntNum = 1000 + endIntNum + 1;
            //把10000的1去掉，获取到最新的后四位
            String newEndNum = String.valueOf(newEndIntNum).substring(1);
            //生成单号
            newOddNumber = formatDate + newEndNum;
            //将单号返回
            return newOddNumber;
        } else {
            //如果为空(第一次生成)或者当前最大订单号的日期与当前日期不一致说明需要重新计数生成单号
            newOddNumber = formatDate + "001";
            //将单号返回
            return newOddNumber;
        }
    }

}
