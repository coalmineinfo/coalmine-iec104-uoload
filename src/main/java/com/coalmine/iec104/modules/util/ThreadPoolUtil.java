package com.coalmine.iec104.modules.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.concurrent.CustomizableThreadFactory;

import java.util.concurrent.*;

/**
 * @Author mkq
 * @Date 2021-08-25 14:05
 * @Description
 */
@Slf4j
public class ThreadPoolUtil {

    static ThreadFactory dealMsgThreadFactory = new CustomizableThreadFactory("第三方电力设备数据进行转发-pool-");

    static ThreadFactory afterMsgThreadFactory = new CustomizableThreadFactory("第三方电力设备数据进行转发-pool-");


    /**
     * 消息解析后处理
     */
    public static ExecutorService dealUploadService = new ThreadPoolExecutor(2, 2,
            0L, TimeUnit.MINUTES,
            new LinkedBlockingQueue<>(), dealMsgThreadFactory);

    /**
     * 消息解析后处理
     */
    public static ExecutorService afterUploadService = new ThreadPoolExecutor(2, 2,
            0L, TimeUnit.MINUTES,
            new LinkedBlockingQueue<>(), afterMsgThreadFactory);


}
